import olympiad;
size(130);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, Y, Z;
path c, cc;

A = origin;
B = (100, 0);
C = (100, 60);
D = (0, 60);

M = (A + D)/2;
NN = (B + C)/2;

draw(A--B--C--D--cycle);
filldraw(B--M--D--NN--cycle, gray, black);

label("$A$",A,SW);
label("$B$",B,SE);
label("$C$",C,NE);
label("$D$",D,NW);
label("$M$",M,W);
label("$N$",NN,E);
