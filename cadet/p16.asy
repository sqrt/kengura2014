import olympiad;
size(140);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, Y, Z;
path c, cc;

C = origin;
B = (100, 0);
A = (20, 80);
H = foot(B, A, C);
X = bisectorpoint(C, A, B);
D = extension(A, X, C, B);
Y = extension(A,D,B,H);
draw(rightanglemark(B,H,A,200));

draw(anglemark(C,A,D,500));
draw(anglemark(D,A,B,550));
draw(anglemark(B,Y,A,150,200));

draw(A--C--B--cycle);
draw(A--D);
draw(B--H);

label("$A$",A,NW);
label("$B$",B,SE);
label("$C$",C,SW);
label("$D$",D,S);

label("$H$",H,W+N/2);

label("$\alpha$",A,(0,-6.2));
label("$\alpha$",A,(3.2,-5.5));
label("$4\alpha$",Y,(1.5,1.5));
