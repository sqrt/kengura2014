import olympiad;
size(100);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, Y, Z;
path c, cc;
O = origin;
A = (110, 0);
B = (0, 60);
C = A+B;

X = B + B*(0, -1);
Y = C + B - X;


draw(anglemark(Y,A,O,500));
draw(anglemark(A,O,X,500));

draw(O--A--C--B--cycle);
draw(O--X);
draw(A--Y);

label("$45^\circ$",O,(4,1.5));
label("$45^\circ$",A,(-3.5,1.5));
label("11",(A+O)/2,(0,-1));
label("6",(C+A)/2,(1,0));
