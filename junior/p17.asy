import olympiad;
size(250);
pair O, P, A, B, C, X, D, E, Y, BB, CC, AA, PB, PC, PA;
path c;
O = origin;
A = (-40, 0);
P = (140, 0);
c=circle(O,40);
B = tangent(P, O, 40, 1);
draw(c);
X = bisectorpoint(A, P, B);
C = extension(P, X, A, B);

BB = (B-P)/5 + B;
AA = (A-P)/20 + A;
CC = (C-P)/5 + C;

PB = -(B-P)/10 + P;
PA = -(A-P)/12 + P;
PC = -(C-P)/10 + P;

dot(O);
dot(C);

draw(A--B);
draw(PB--BB);
draw(PA--AA);

label("$A$",A,SW);
label("$B$",B,N);
label("$C$",C,N);
label("$P$",P,S);
label("$O$",O,S);
