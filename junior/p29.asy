import olympiad;
size(170);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, Y, Z;
path c;

B = origin;
C = (100,0);
A = (36, 48);
M = B + C / 2;
D = M + (A-M)*(0,-1);
EE = D + A - M;
F = extension(M, D, A, C);

draw(A--B--C--cycle);
draw(A--M--D--EE--cycle);

label("$A$",A,N);
label("$B$",B,SW);
label("$C$",C,SE);
label("$M$",M,S);
label("$D$",D,SE);
label("$E$",EE,NE);
label("$F$",F,(-0.4, -1.4));

filldraw(A--F--D--EE--cycle, lightgray);
