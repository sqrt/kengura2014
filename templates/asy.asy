import olympiad;
size(250);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, Y, Z;
path c, cc;
O = origin;
A = (-40, 0);
P = (140, 0);
c=circle(O,40);
B = tangent(P, O, 40, 1);
draw(c);
X = bisectorpoint(A, P, B);
Y = (B-P)/5 + B;
C = extension(P, X, A, B);

draw(rightanglemark(O,B,P,200));

draw(anglemark(C,P,O,1000));
draw(anglemark(B,P,C,1050));

draw(A--P--B--cycle);
draw(P--C);
draw(B--O);
draw(P--Y);

label("$A$",A,SW);
label("$B$",B,N);
label("$C$",C,N);
label("$P$",P,SE);
label("$O$",O,S);

label("$\alpha$",A,(3,1));

add(pathticks(O--A,2,0.5,30,100));
add(pathticks(O--B,2,0.45,30,100));
