import os



def get_problems(text):
    problems = []
    while get_first_token(text):
        token = get_first_token(text)
        piece, text = separate_first_piece(text, token)
        if token != '\\kvoid':
            problems.append(piece)
        else:
            problems[-1] = problems[-1] + piece
    return problems

def get_first_token(text):
    tex = text.find("\\ktext")
    pic = text.find("\\kpic")
    void = text.find("\\kvoid")
    if tex+pic+void == -3:
        return None
    else:
        l = [(score(tex), '\\ktext'), (score(pic), '\\kpic'), (score(void), '\\kvoid')]
        return min(l)[1]

def score(number):
    if number == -1:
        return 10000
    else:
        return number

def separate_first_piece(text, token):
    start = text.find(token)
    text = text[start:]
    pro = ""
    cycles = {"\\ktext": 2, "\\kpic": 5, "\\kvoid": 1}
    for i in range(cycles[token]):
        a, text = get_bracketed(text)
        pro += '{'+a+'}'
    return token+pro, text


def get_bracketed(string, op = '{', cl = '}'):
    if string[0] != op:
        start = string.find(op)
        #print "Loosing" + string[:start+1],
        string = string[start+1:]
    else:
        string = string[1:]

    if cl not in string:
        print "Aborting, misformatted string"
        return 

    level = 1
    position = 0
    while level > 0:
        if string[position] == op:
            level += 1
        if string[position] == cl:
            level -= 1
        position += 1

    return string[:position-1], string[position:]

groupfull = (os.getcwd().split('/'))[-1]
en = open(groupfull + "_en.tex", 'r')
answers = open("../" + groupfull +"_answers.tex", 'r')

#Do easy ones
lt = open(groupfull + "_lt.tex", 'w')
ru = open(groupfull + "_ru.tex", 'w')
pl = open(groupfull + "_pl.tex", 'w')

text = en.read()
text_lt = text[:]
text_ru = text[:]
text_pl = text[:]

text_lt=text_lt.replace("EN", "LT")
text_lt=text_lt.replace("pointsen", "pointslt")
text_lt=text_lt.replace("greeting{en}", "greeting{lt}")

text_ru=text_ru.replace("EN", "RU")
text_ru=text_ru.replace("pointsen", "pointsru")
text_ru=text_ru.replace("greeting{en}", "greeting{ru}")

text_pl=text_pl.replace("EN", "PL")
text_pl=text_pl.replace("pointsen", "pointspl")
text_pl=text_pl.replace("greeting{en}", "greeting{pl}")

ru.write(text_ru)
pl.write(text_pl)
lt.write(text_lt)

lt.close()
ru.close() 
pl.close()


#Do hard one
_all = open(groupfull + "_all.tex", 'w')

all_header=("\\documentclass[10pt,a4paper,oneside]{memoir}\n"
            "\\pdfoutput=1\n"
            "\\usepackage{../other/kengura}\n"
            "\\input{../%s_problems}\n"
            "\\input{../%s_answers}\n"
            "\\begin{document}\n") % (groupfull, groupfull)

all_footer="\\end{document}"

_all.write(all_header)

problems = get_problems(text)
for problem in problems:
    _all.write("\\vbox{\n"
               "%s\n\n\\bigskip"
               "%s\n\n\\bigskip"
               "%s\n\n\\bigskip"
               "%s\n\n\\bigskip"
               "}\n\\newpage\n" % (problem, 
                        problem.replace("EN", "LT"),
                        problem.replace("EN", "PL"), 
                        problem.replace("EN", "RU")) )

_all.write(all_footer)

_all.close()
