import olympiad;
size(140);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, OO, P, R, SS, T, U, V, WW, X, Y, Z;
path c, cc;

O = origin;
c=circle(O,40);
OO = O + 80;
cc = circle(OO, 40);

I = (32, 0-40);
J = (48, 0-40);
K = (32, 16-40);
L = (48, 16-40);

draw(c);
draw(cc);
draw(I--J--L--K--cycle);
draw((-50,-40)--(130,-40));
