import fileinput
import os

#Global number dictionary
N2T = {'1':'one', '2':'two', '3':'three', '4':'four', '5':'five', '6':'six', '7':'seven', '8':'eigth', '9':'nine', '10':'ten',
       '11':'uone', '12':'utwo', '13':'uthree', '14':'ufour', '15':'ufive', '16':'usix', '17':'useven', '18':'ueigth', '19':'unine', '20':'uten',
       '21':'done', '22':'dtwo', '23':'dthree', '24':'dfour', '25':'dfive', '26':'dsix', '27':'dseven', '28':'deigth', '29':'dnine', '30':'dten'}

def bboxfinder(filename):
    try:
        f = open(filename+'.pdf')
    except IOError:
        cmd = 'convert -quality 100 %s.png %s.pdf' %(filename, filename)
        os.system(cmd)
        f = open(filename+'.pdf')

    for line in f:
        start = max( line.find('MediaBox'), line.find('BBox') )
        if start > -1:
            dimensions, _ = get_bracketed(line[start:], '[', ']')
            return int(dimensions.split()[2].split('.')[0])+1

def parse_problem(problem):
    number, problem = get_bracketed(problem)
    number = number.split()[0]
    _, problem = get_bracketed(problem)
    text, problem = get_bracketed(problem)
    a1, problem = get_bracketed(problem)
    a2, problem = get_bracketed(problem)
    a3, problem = get_bracketed(problem)
    a4, problem = get_bracketed(problem)
    a5, problem = get_bracketed(problem)

    images, text = get_images(text)
    widths = [bboxfinder(image) for image in images]
    answers = '{' + '}{'.join((a1, a2, a3, a4, a5)) + '}'
    text = ' '.join(text.split())

    return (number, text, answers, images, widths)

def form_header(group, number):
    cnt = ("%%%s%s" % (group, number))
    spacer = "="*(1 - int(number)/10)
    return cnt+spacer+30*"="+'['+int(number)*'='+(30-int(number))*'.'+']'+30*"="+spacer+cnt+"\n"

def form_problems(raw_data, group):
    number, text, _, _, _ = raw_data
    head = form_header(group, number)
    number = N2T[number]
    en = "\def\\%s%sEN{%s}\n" % (group, number, text)
    lt = "\def\\%s%sLT{}\n" % (group, number)
    pl = "\def\\%s%sPL{}\n" % (group, number)
    ru = "\def\\%s%sRU{}\n" % (group, number)

    return head + en + lt + pl + ru + '\n'

def form_answers(raw_data, group):
    number, _, answers, _, _ = raw_data
    head = form_header(group, number)
    number = N2T[number]

    #guess if answer is language specific
    if all(x not in answers for x in 'abcdefghijklmnoprstuvwxyz'):
        return head + "\def\\a%s%s{\\answer%s}\n\n" % (group, number, answers)
    else:
        en = "\def\\a%s%sEN{\\answer%s}\n" % (group, number, answers)
        lt = "\def\\a%s%sLT{\\answer}\n" % (group, number)
        pl = "\def\\a%s%sPL{\\answer}\n" % (group, number)
        ru = "\def\\a%s%sRU{\\answer}\n" % (group, number)
        return head + en + lt + pl + ru + '\n'

def form_en(raw_data,group):
    number, _, answers, images, widths = raw_data
    WIDE_IMAGE = 200

    #guess if answer is language specific
    if all(x not in answers for x in 'abcdefghijklmnoprstuvwxyz'):
        answers = "\\a%s%s" % (group, N2T[number])
    else:
        answers = "\\a%s%sEN" % (group, N2T[number])

    text = "\\%s%sEN" % (group, N2T[number])

    #case no image
    if not images:
        return "\ktext{%s}{%s\\newline%s}\n" % (number, text, answers)
    #case one normal image
    elif len(images) == 1 and widths[0] < WIDE_IMAGE:
        return "\kpic{%s}{%s\\newline%s}{%s}{%dpt}{1}\n" % (number, text, answers, images[0], widths[0]) 
    #case one long image
    elif len(images) == 1 and widths[0] >= WIDE_IMAGE:
        return ("\ktext{%s}{\n"
                "%s\n"
                "\\vspace{-5pt}\n"
                "\\begin{center} \\includegraphics[scale=1]{%s} \\end{center}\n"
                "\\vspace{-5pt}\n"
                "%s}\n") % (number, text, images[0], answers)
    #case more than one image
    else:
        return ("\kpic{%s}{%s\\newline%s}{%s}{%dpt}{1}\n"
                "%%plociai%s\n") % (number, text, answers, images[0],
                                    widths[0],str(widths)) 
    return

def get_images(text):
    new = ""
    images = []
    while '\\includegraphics' in text:
        start = text.find('\\includegraphics')
        new += text[:start]
        text = text[start:]
        o_br = text.find('{')
        c_br = text.find('}')
        images.append(text[o_br+1: c_br])
        text = text[c_br+1:]
    new += text
    return images, new

def get_bracketed(string, op = '{', cl = '}'):
    if string[0] != op:
        start = string.find(op)
        #print "Loosing" + string[:start+1],
        string = string[start+1:]
    else:
        string = string[1:]

    if cl not in string:
        print "Aborting, misformatted string:"
        print string
        return 

    level = 1
    position = 0
    while level > 0:
        if string[position] == op:
            level += 1
        if string[position] == cl:
            level -= 1
        position += 1

    return string[:position-1], string[position:]

def test_function():
    assert get_bracketed('{aaa}bbb') == ('aaa', 'bbb')

if __name__ == '__main__':

    #read the official kengura file and separate out the problems
    megalist = list(fileinput.input())
    megalist = [x.strip() for x in megalist]
    megastring = ' '.join(megalist)
    probs = megastring.split('\\begin{document}')[1].split('\problemID')[1:]

    #get the group
    groupfull = (os.getcwd().split('/'))[-1]
    group = groupfull[:1].upper()
    groupnumber = {'P': 1, 'E':2, 'B':3, 'C':4, 'J':5, 'S':6}

    en_header=("\\documentclass[10pt,a4paper,twocolumn,landscape,oneside]{memoir}\n"
               "\\pdfoutput=1\n"
               "\\usepackage{../other/kengura}\n"
               "\\input{../%s_problems}\n"
               "\\input{../%s_answers}\n"
               "\\begin{document}\n") % (groupfull, groupfull)

    en_footer =("%%Put us somewhere\n"
                "\\pointsen{3}\n"
                "\\pointsen{4}\n"
                "\\pointsen{5}\n"
                "\\greeting{en}{%d}\n"
                "\\end{document}\n") % (groupnumber[group]) 

    problems = open(groupfull + "_problems.tex", 'w')
    answers = open(groupfull + "_answers.tex", 'w')
    en = open(groupfull + "_enX.tex", 'w')

    #writeout headers
    en.write(en_header)

    #parse, format and writeout each problem
    for problem in probs:
        raw_content = parse_problem(problem)

        problems_string = form_problems(raw_content, group)
        answers_string = form_answers(raw_content, group)
        en_string = form_en(raw_content, group)

        problems.write(problems_string)
        answers.write(answers_string)
        en.write(en_string)

    #writeout footers
    en.write(en_footer)
    
    problems.close()
    answers.close()
    en.close()
