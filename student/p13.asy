import olympiad;
size(120);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, N, O, P, R, S, T, U, V, WW, X, Y, Z;
path c, cc;
O = origin;
c=circle(O,30);
cc=circle(O,100);
C=waypoint(cc, 0);
A=2*O - C;
X = tangent(C, O, 30, 2);
B = 2*X - C;

draw(c);
draw(cc);
draw(A--C);
draw(B--C);
dot(O);

label("$A$",A,W);
label("$B$",B,(SW+W)/1.4);
label("$C$",C,E);
