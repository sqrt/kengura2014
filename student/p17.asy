import olympiad;
size(80);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, Y, Z;
path c, cc;

A = origin;
B = (100, 0);
C = (40, 20);
D = B+C;

I = (0,100);
J = I + B;
K = I + C;
L = I + D;

draw(A--B--D--L--K--I--J--L);
draw(J--B);
draw(I--A);

draw(A--C--K, dashed);
draw(C--D, dashed);

label("$1$",A,S);
label("$4$",B,S);
label("$6$",C,NE);
label("$x$",L,N);
