import olympiad;
size(150);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, XX, Y, Z;
path c, cc;
A = origin;
B = (0, 50);
D = (100, 0);
C = B + D;
X = (110, 80);
XX = -X/10;
Y = foot(C, A, X);
Z = foot(D, A, X);

draw(A--B--C--D--cycle);
draw(XX--X);
draw(C--Y, dashed);
draw(D--Z, dashed);

label("$A$",A,S);
label("$B$",B,N);
label("$C$",C,NE);
label("$D$",D,SE);
label("$l$",X,SW*4 + W*6);
