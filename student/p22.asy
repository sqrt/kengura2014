import olympiad;
size(120);
pair A, B, C, D, EE, F, G, H, I, J, K, L, M, NN, O, P, R, SS, T, U, V, WW, X, XX, Y, Z;
path c, cc;
A = origin;
B = (0, 70);
D = (100, 0);
C = B + D;
T = (B + C)/2;

draw(A--B--C--D--cycle);
draw(T--D);
draw(A--C);

label("$P$",A,S);
label("$S$",B,N);
label("$R$",C,N);
label("$Q$",D,S);
label("$T$",T,N);
